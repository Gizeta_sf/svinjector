#include <vector>
#include <Windows.h>
#include "../Blackbone/src/BlackBone/Process/Process.h"
#include "../Blackbone/src/BlackBone/Process/RPC/RemoteFunction.hpp"

using namespace std;

#define INJECT_SUCCESS 0

#define TARGET_PROCESS_NOT_FOUND            101
#define MULTIPLE_TARGETS                    102
#define BARRIER_UNEXPECTED                  201
#define MONO_MODULE_NOT_FOUND               202
#define MONO_DOMAIN_GET_NOT_FOUND           301
#define MONO_DOMAIN_GET_UNAVAILABLE         302
#define MONO_ASSEMBLY_OPEN_NOT_FOUND        303
#define MONO_ASSEMBLY_OPEN_UNAVAILABLE      304
#define ASSEMBLY_LOAD_FAILED                305
#define MONO_ASSEMBLY_GET_IMAGE_NOT_FOUND   306
#define MONO_ASSEMBLY_GET_IMAGE_UNAVAILABLE 307
#define MONO_CLASS_FROM_NAME_NOT_FOUND      308
#define MONO_CLASS_FROM_NAME_UNAVAILABLE    309
#define MONO_CLASS_GET_METHOD_NOT_FOUND     310
#define MONO_CLASS_GET_METHOD_UNAVAILABLE   311
#define MONO_RUNTIME_INVOKE_NOT_FOUND       312
#define MONO_RUNTIME_INVOKE_UNAVAILABLE     313
#define READ_ASSEMBLY_FAILED                401

typedef void*(__cdecl* mono_domain_get)();
typedef void*(__cdecl* mono_assembly_open)(const char *filename, int *status);
typedef void*(__cdecl* mono_assembly_get_image)(void *assembly);
typedef void*(__cdecl* mono_class_from_name)(void *image, const char *name_space, const char *name);
typedef void*(__cdecl* mono_class_get_method_from_name)(void *klass, const char* name, int param_count);
typedef void*(__cdecl* mono_runtime_invoke)(void *method, void *obj, void **params, int **exc);

extern "C" _declspec(dllexport) long Inject(const char* dllName, const char* methodName) {
	vector<DWORD> foundPIDs = blackbone::Process::EnumByName(L"Shadowverse.exe");

	if (foundPIDs.size() == 0) return TARGET_PROCESS_NOT_FOUND;
	if (foundPIDs.size() > 1) return MULTIPLE_TARGETS;
	
	blackbone::Process targetProcess;
	NTSTATUS attached = targetProcess.Attach(foundPIDs[0]);

	if (!NT_SUCCESS(attached)) return attached;
	
	auto barrier = targetProcess.core().native()->GetWow64Barrier().type;
	if (barrier != blackbone::wow_32_32 && barrier != blackbone::wow_64_64) return BARRIER_UNEXPECTED;
	
	const auto targetModule = targetProcess.modules().GetModule(L"mono.dll");
	if (targetModule == nullptr) return MONO_MODULE_NOT_FOUND;
	
	//

	auto mono_domain_get_addr = targetProcess.modules().GetExport(targetModule, "mono_domain_get");
	if (mono_domain_get_addr->procAddress == 0) return MONO_DOMAIN_GET_NOT_FOUND;
	
	blackbone::RemoteFunction<mono_domain_get> rpc_mono_domain_get(targetProcess, mono_domain_get_addr->procAddress);
	auto domain_result = rpc_mono_domain_get.Call(targetProcess.threads().getMain());
	if (!domain_result.success() || domain_result.result() == nullptr) return MONO_DOMAIN_GET_UNAVAILABLE;
	
	//
	
	auto mono_assembly_open_addr = targetProcess.modules().GetExport(targetModule, "mono_assembly_open");
	if (mono_assembly_open_addr->procAddress == 0) return MONO_ASSEMBLY_OPEN_NOT_FOUND;
	
	int status;
	blackbone::RemoteFunction<mono_assembly_open> rpc_mono_assembly_open(targetProcess, mono_assembly_open_addr->procAddress);
	auto open_result = rpc_mono_assembly_open.Call(dllName, &status, targetProcess.threads().getMain());
	if (!open_result.success() || open_result.result() == nullptr) return MONO_ASSEMBLY_OPEN_UNAVAILABLE;
	if (status != 0) return ASSEMBLY_LOAD_FAILED;
	
	auto mono_assembly_get_image_addr = targetProcess.modules().GetExport(targetModule, "mono_assembly_get_image");
	if (mono_assembly_get_image_addr->procAddress == 0) return MONO_ASSEMBLY_GET_IMAGE_NOT_FOUND;
	
	blackbone::RemoteFunction<mono_assembly_get_image> rpc_mono_assembly_get_image(targetProcess, mono_assembly_get_image_addr->procAddress);
	auto image_result = rpc_mono_assembly_get_image.Call(open_result.result(), targetProcess.threads().getMain());
	if (!image_result.success() || image_result.result() == nullptr) return MONO_ASSEMBLY_GET_IMAGE_UNAVAILABLE;

	//
	
	auto mono_class_from_name_addr = targetProcess.modules().GetExport(targetModule, "mono_class_from_name");
	if (mono_class_from_name_addr->procAddress == 0) return MONO_CLASS_FROM_NAME_NOT_FOUND;
	
	blackbone::RemoteFunction<mono_class_from_name> rpc_mono_class_from_name(targetProcess, mono_class_from_name_addr->procAddress);
	auto class_result = rpc_mono_class_from_name.Call(image_result.result(), "ShadowWatcher", "Loader", targetProcess.threads().getMain());
	if (!class_result.success() || class_result.result() == nullptr) return MONO_CLASS_FROM_NAME_UNAVAILABLE;

	//

	auto mono_class_get_method_addr = targetProcess.modules().GetExport(targetModule, "mono_class_get_method_from_name");
	if (mono_class_get_method_addr->procAddress == 0) return MONO_CLASS_GET_METHOD_NOT_FOUND;
	
	blackbone::RemoteFunction<mono_class_get_method_from_name> rpc_mono_class_get_method(targetProcess, mono_class_get_method_addr->procAddress);
	auto method_result = rpc_mono_class_get_method.Call(class_result.result(), methodName, 0, targetProcess.threads().getMain());
	if (!method_result.success() || method_result.result() == nullptr) return MONO_CLASS_GET_METHOD_UNAVAILABLE;

	//

	auto mono_runtime_invoke_addr = targetProcess.modules().GetExport(targetModule, "mono_runtime_invoke");
	if (mono_runtime_invoke_addr->procAddress == 0) return MONO_RUNTIME_INVOKE_NOT_FOUND;
	
	blackbone::RemoteFunction<mono_runtime_invoke> rpc_mono_runtime_invoke(targetProcess, mono_runtime_invoke_addr->procAddress);
	auto invoke_result = rpc_mono_runtime_invoke.Call(method_result.result(), NULL, NULL, NULL, targetProcess.threads().getMain());
	if (!invoke_result.success()) return MONO_RUNTIME_INVOKE_UNAVAILABLE;
	
	return INJECT_SUCCESS;
}